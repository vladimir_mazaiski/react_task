import React from "react";
import { Switch, Route } from "react-router-dom";
import HomePage from "../../pages/HomePage";
import EmployeesPage from "../../pages/EmployeesPage";

const App = () => {
    return (
        <div>
            <Switch>
                <Route path='/' exact>
                    <HomePage />
                </Route>
                <Route path='/employees'>
                    <EmployeesPage />
                </Route>
            </Switch>
        </div>
    )
}

export default App;