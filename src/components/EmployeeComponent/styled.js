import styled from 'styled-components';

const ItemStyle = styled.div`
    position: relative;
    width: 350px;
    display: flex;
    margin-bottom: 15px;
    border: 1px solid #dedede;
    padding: 10px;
    border-radius: 6px;
`

const ItemImgStyle = styled.img`
    width: 100px;
    height: 100px;
    border-radius: 6px;
    margin-right: 10px;
    object-fit: cover;
`

const ItemInfoStyle = styled.div`
    display: flex;
    flex-direction: column;
`
const ItemInfoNameStyle = styled.div`
    margin-bottom: 5px;
`
const ItemInfoCrossStyle = styled.div`
    position: absolute;
    top: 7px;
    right: 10px;
    cursor: pointer;
`

export {
    ItemStyle,
    ItemImgStyle,
    ItemInfoStyle,
    ItemInfoNameStyle,
    ItemInfoCrossStyle
};