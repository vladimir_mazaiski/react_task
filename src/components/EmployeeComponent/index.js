import React from 'react';
import {
    ItemStyle,
    ItemImgStyle,
    ItemInfoStyle,
    ItemInfoNameStyle,
    ItemInfoCrossStyle
} from './styled';

const EmployeeComponent = ({
    item,
    removeItem,
}) => {
    return (
        <ItemStyle>
            <ItemImgStyle src={item.avatar} alt={item.last_name} />
            <ItemInfoStyle>
                <ItemInfoNameStyle>{item.first_name} {item.last_name}</ItemInfoNameStyle>
                <div>{item.email}</div>
            </ItemInfoStyle>
            <ItemInfoCrossStyle onClick={() => removeItem(item.id)}>&#10006;</ItemInfoCrossStyle>
        </ItemStyle>
    )
}

export default EmployeeComponent;