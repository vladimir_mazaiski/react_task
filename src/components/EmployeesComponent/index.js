import React, { useState } from 'react';
import EmployeeComponent from '../EmployeeComponent';
import {
    EmployeesWrappStyle,
    EmployeesStyle,
    EmployeesFormStyle,
} from './styled';

const EmployeesComponent = ({
    employees,
    addItem,
    removeItem
}) => {
    const date = new Date().getTime();

    const [formData, getFormFields] = useState({
        id: date,
        avatar: "https://upload.wikimedia.org/wikipedia/commons/8/89/Portrait_Placeholder.png",
        firstName: '',
        lastName: '',
        email: 'test@test.com',
    });

    const [inputError, getInputError] = useState({
        firstName: {
            error: false,
            errorMessage: 'Введите имя'
        },
        lastName: {
            error: false,
            errorMessage: 'Введите фамилию'
        }
    });

    const inputHandler = (e, name) => {
        getFormFields({ ...formData, [name]: e.target.value })
    }

    const validationForm = () => {
        let isCheck = true;
        for (const key in formData) {
            const val = formData[key];
            switch (key) {
                case key:
                    if (val === '') {
                        isCheck = false;
                        getInputError((prevInputError) => (
                            {
                                ...prevInputError,
                                [key]:
                                {
                                    ...prevInputError[key],
                                    error: true
                                }
                            }
                        ))
                    } else {
                        getInputError((prevInputError) => (
                            {
                                ...prevInputError,
                                [key]:
                                {
                                    ...prevInputError[key],
                                    error: false
                                }
                            }
                        ));
                    }
                    break;
                default:
                    break;
            }
        }
        return isCheck;
    }

    const addNewItem = () => {
        const isValid = validationForm();
        if (!isValid) return false;

        getFormFields({ ...formData, id: date });
        addItem(formData);
    }

    return (
        <EmployeesWrappStyle>
            <EmployeesStyle>
                {employees.map((item) => {
                    return <EmployeeComponent
                        key={item.id}
                        item={item}
                        removeItem={removeItem}
                    />
                })}
            </EmployeesStyle>
            <EmployeesFormStyle onSubmit={e => { e.preventDefault() }}>
                <label>
                    {inputError.firstName.error && <div>{inputError.firstName.errorMessage}</div>}
                    <input type='text' placeholder='Имя*' name='firstName' value={formData.firstName}
                        onChange={(e) => inputHandler(e, e.target.name)} />
                </label>
                <label>
                    {inputError.lastName.error && <div>{inputError.lastName.errorMessage}</div>}
                    <input type='text' placeholder='Фамилия*' name='lastName' value={formData.lastName}
                        onChange={(e) => inputHandler(e, e.target.name)}
                    />
                </label>
                <button onClick={() => addNewItem()}>Добавить</button>
            </EmployeesFormStyle>
        </EmployeesWrappStyle>
    )
}

export default EmployeesComponent;