import styled from 'styled-components';

const EmployeesWrappStyle = styled.div`
    display: flex;
`

const EmployeesStyle = styled.div`
    display: flex;
    flex-direction: column;
`

const EmployeesFormStyle = styled.form`
    display: flex;
    flex-direction: column;
    padding: 0 50px;

    input {
        border: 1px solid #dedede;
        border-radius: 6px;
        padding: 10px;
        margin-bottom: 10px;
    }

    button {
        background-color: #54c94a;
        border: none;
        border-radius: 6px;
        padding: 10px;
        color: #fff;
        cursor: pointer;
    }
`

export {
    EmployeesWrappStyle,
    EmployeesStyle,
    EmployeesFormStyle,
};