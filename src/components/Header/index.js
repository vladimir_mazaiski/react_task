import React from 'react';
import { Link } from 'react-router-dom';
import { HeaderMenuStyle } from './styled';

const Header = () => {
    return (
        <div>
            <HeaderMenuStyle>
                <li>
                    <Link to='/'>Главная</Link>
                </li>
                <li>
                    <Link to='/employees'>Сотрудники</Link>
                </li>
            </HeaderMenuStyle>
        </div>
    )
}

export default Header;