import styled from 'styled-components';

const HeaderMenuStyle = styled.ul`
    display: flex;
    list-style: none;
    padding: 0;

    li {
        margin-right: 10px;
    }

    a {
        text-decoration: none;
        color: #000;
    }
`

export {
    HeaderMenuStyle,
};