export default class api {
    _apiBase = 'https://reqres.in/api/';

    async getData(url) {
        const response = await fetch(`${this._apiBase}${url}`);
        return await response.json();
    }

    async getEmployees() {
        const response = await this.getData('users?per_page=12');
        return response.data;
    }
}