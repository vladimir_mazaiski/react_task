import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import store from './store';
import { ServiceProvider } from './components/Context';
import Api from './services/api'
import App from './components/App';

const api = new Api();

ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
      <ServiceProvider value={api}>
        <App />
      </ServiceProvider>
    </BrowserRouter>
  </Provider>,
  document.getElementById('root')
);