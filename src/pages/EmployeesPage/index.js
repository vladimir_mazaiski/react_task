import React from "react";
import { ServiceConsumer } from "../../components/Context";
import Header from "../../components/Header";
import EmployeesContainer from "../../container/EmployeesContainer";

const EmployeesPage = () => {
    return (
        <div>
            <Header />
            <ServiceConsumer>
                {
                    (api) => {
                        return (
                            <EmployeesContainer api={api} />
                        )
                    }
                }
            </ServiceConsumer>
        </div>
    )
}

export default EmployeesPage;