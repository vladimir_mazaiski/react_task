import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import {
    fetchDataRequested,
    fetchDataSuccess,
    addDataItem,
    removeDataItem,
} from '../../actions';
import EmployeesComponent from '../../components/EmployeesComponent';

const EmployeesContainer = ({
    api,
    fetchReauest,
    fetchSuccess,
    employees,
    addItem,
    removeItem,
}) => {

    useEffect(() => {
        fetchReauest()
        api.getEmployees()
            .then(resp => fetchSuccess(resp))
    }, [
        api,
        fetchReauest,
        fetchSuccess
    ]);

    return (
        <div>
            {
                employees.loading
                    ? <div>Загрузка</div>
                    : <EmployeesComponent
                        employees={employees.items}
                        addItem={addItem}
                        removeItem={removeItem}
                    />
            }
        </div>
    )
}

const mapStateToProps = (state) => {
    const { employees } = state;

    return {
        employees
    };
};

const mapDisapatchToProps = (dispatch) => {
    return {
        fetchReauest: () => dispatch(fetchDataRequested()),
        fetchSuccess: (newData) => dispatch(fetchDataSuccess(newData)),
        addItem: (newItem) => dispatch(addDataItem(newItem)),
        removeItem: (id) => dispatch(removeDataItem(id)),
    }
};


export default connect(mapStateToProps, mapDisapatchToProps)(EmployeesContainer);