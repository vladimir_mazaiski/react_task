const initialState = {
    employees: {
        items: [],
        loading: true
    }
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case 'FETCH_DATA_REQUESTED':
            return {
                ...state,
                employees: {
                    items: [],
                    loading: true
                },

            }
        case 'FETCH_DATA_SUCCESS':
            return {
                ...state,
                employees: {
                    items: action.payload,
                    loading: false
                }
            }
        case 'ADD_ITEM':
            const newItem = action.payload;
            return {
                ...state,
                employees: {
                    items: [
                        {
                            avatar: newItem.avatar,
                            email: newItem.email,
                            first_name: newItem.firstName,
                            id: newItem.id,
                            last_name: newItem.lastName,
                        },
                        ...state.employees.items
                    ],
                }
            };
        case 'REMOVE_ITEM':
            return {
                ...state,
                employees: {
                    items: state.employees.items.filter((item) => item.id !== action.payload)
                }
            };
        default: {
            return state;
        }
    }
};

export default reducer;