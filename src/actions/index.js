const fetchDataRequested = () => {
    return {
        type: 'FETCH_DATA_REQUESTED',
    }
}

const fetchDataSuccess = (newData) => {
    return {
        type: 'FETCH_DATA_SUCCESS',
        payload: newData
    }
}

const addDataItem = (newItem) => {
    return {
        type: 'ADD_ITEM',
        payload: newItem
    }
}

const removeDataItem = (id) => {
    return {
        type: 'REMOVE_ITEM',
        payload: id
    }
}

export {
    fetchDataRequested,
    fetchDataSuccess,
    addDataItem,
    removeDataItem,
}